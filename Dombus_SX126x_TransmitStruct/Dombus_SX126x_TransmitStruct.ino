/*
   RadioLib SX126x Transmit Example

   This example transmits packets using SX1262 LoRa radio module.
   Each packet contains up to 256 bytes of data, in the form of:
    - Arduino String
    - null-terminated char array (C-string)
    - arbitrary binary data (byte array)

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

#include <Wire.h>              //I2C väylä näytölle
#include <Adafruit_GFX.h>      //Grafiikkakirjasto
#include <Adafruit_SSD1306.h>  //Näytön ajurikirjasto

Adafruit_SSD1306 display(-1);

// include the library
#include <RadioLib.h>

int globalLine = 0;
int globalCol = 0;

// == Inputs ==
const int buttonADCPin = PA0;
const int dallasPin = PB10;
const int fan1TachoPin = PB12;
const int fan2TachoPin = PB13;

const int Opto1Pin = PC15;
const int Opto2Pin = PC14;

// == Comms ==
const int LoRa_RXEN = PB14;
const int LoRa_TXEN = PB15;
const int LoRa_DIO1 = PB2;
const int LoRa_DIO2 = PA11;
const int LoRa_NRST = PA15;
const int LoRa_BUSY = PA4;
const int LoRa_NSS = PB3;
const int LoRa_MOSI = PA7; //"SPI1"
const int LoRa_MISO = PA6; //"SPI1"
const int LoRa_SCK = PA5;  //"SPI1"

const int RS485_RE_DE = PA8;
const int RS485_RX = PA9; //"Serial 1"
const int RS485_TX = PA10; //"Serial 1"
const int RX2Pin = PA3;  //"Serial 2"
const int TX2Pin = PA2;  //"Serial 2"

// == Outputs ==
const int fan1PWMPin = PB9;
const int fan2PWMPin = PB8;
const int ledPin = PC13;
const int relay1Pin = PB5;
const int relay2Pin = PB4;
const int AUX1Pin = PB1;
const int AUX2Pin = PB0;
const int AUX3PinLed = PA1;
const int WS2813_data = PA12;

const int fan1PWMChannel = 4;
const int fan2PWMChannel = 3;

// SX1262 has the following connections:
// NSS pin:   PB3
// DIO1 pin:  PB2
// NRST pin: PA15
// BUSY pin:  PA4
int debug1 = 0;

//SPIClass SPI_1(PA7, PA6, PA5);
//SPISettings spiSettings(2000000, MSBFIRST, SPI_MODE0);
//SX1278 radio = new Module(cs, irq, rst, gpio, spi, spiSettings);

LLCC68 radio = new Module(LoRa_NSS, LoRa_DIO1, LoRa_NRST, LoRa_BUSY);
//LLCC68 radio = new Module(PB3, PB2, PA15, PA4, SPI_1, spiSettings);

struct __attribute__((packed, aligned(4))) EM340_struct{
  int32_t V_L1;
  int32_t V_L2;
  int32_t V_L3;
  int32_t A_L1;
};

EM340_struct EM340 = {12000,555,12,53};
byte transmitBuf[16] = {0};


void setup() {
  Wire.begin();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //Osoite on joko 0x3H tai 0x3C, usein valittavissa näytön takaa.
  display.setRotation(2);

  display.fillScreen(0);       //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
  display.setCursor(0, 0);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
  display.setTextColor(1, 0);  //Tekstin väri
  display.setTextSize(1);      //Tekstin koko, perusfontin koon moninkertana

  // locate devices on the bus
  display.print("Starting ");

  display.display();


  //SPI_1.begin();

  // initialize SX1262 with default settings
  Pront(F("init"));
  //freq = 434.0,bw = 125.0, sf = 9, cr = 7, syncWord = RADIOLIB_SX126X_SYNC_WORD_PRIVATE, power = 10, preambleLength = 8, tcxoVoltage = 1.6, useRegulatorLDO = false);
  int state = radio.begin(434.0, 125.0, 9, 7,RADIOLIB_SX126X_SYNC_WORD_PRIVATE, -9, 8, 0.0, false);
  if (state == RADIOLIB_ERR_NONE) {
    Pront(F("success!"));
  } else {
    Pront(F("failed, "));
    Pront(state);
    while (true);
  }

  // some modules have an external RF switch
  // controlled via two pins (RX enable, TX enable)
  // to enable automatic control of the switch,
  // call the following method
  // RX enable:   PB14
  // TX enable:   PB15
  
    radio.setRfSwitchPins(LoRa_RXEN, LoRa_TXEN);
    //radio.setOutputPower(-9);
  
}

void loop() {
  Pront(F("TP..."));

  // you can transmit C-string or Arduino string up to
  // 256 characters long
  // NOTE: transmit() is a blocking method!
  //       See example SX126x_Transmit_Interrupt for details
  //       on non-blocking transmission method.
  //int state = radio.transmit("Hello World!");
  //int state = radio.transmit((byte*)&EM340,sizeof(EM340));
  //byte byteArr[] = {0x01, 0x23, 0x45, 0x56, 0x78, 0xAB, 0xCD, 0xEF};
  byte byteArr[16] = {0}; byteArr[2] = 4; byteArr[3] = 5;
  //int state = radio.transmit(byteArr, 16);
  int state = SendAnythingLoRa(EM340, sizeof(EM340_struct));

  // you can also transmit byte array up to 256 bytes long
  /*
    byte byteArr[] = {0x01, 0x23, 0x45, 0x56, 0x78, 0xAB, 0xCD, 0xEF};
    int state = radio.transmit(byteArr, 8);
  */

  if (state == RADIOLIB_ERR_NONE) {
    // the packet was successfully transmitted
    //Pront(F("success!"));

    // print measured data rate
    Pront(F("Datarate:"));
    Pront(radio.getDataRate());
    //Pront(debug1);

  } else if (state == RADIOLIB_ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    Pront(F("too long!"));

  } else if (state == RADIOLIB_ERR_TX_TIMEOUT) {
    // timeout occured while transmitting packet
    Pront(F("timeout!"));
    

  } else {
    // some other error occurred
    Pront(F("failed, "));
    Pront(state);

  }

  // wait for a second before transmitting again
  delay(1000);
}

template <class T>
void Pront(T thing) {
  display.setCursor(globalCol * 64, globalLine * 8);
  display.fillRect(globalCol * 64 , globalLine * 8, 64, 8, 0);
  display.print(thing);
  display.drawPixel(globalCol * 64 +62, globalLine * 8 -4, 1);
  display.display();
  globalLine++;
  if (globalLine > 3) {
    globalLine = 0;
    globalCol ++;
  }
  if (globalCol > 1) {
    globalLine = 0;
    globalCol = 0;
  }
}

template <class T> int GetAnything(const T& value, unsigned int len){
    byte* p = (byte*)(void*)&value;
    byte *arr = new byte(len);
    unsigned int i;
    
    int state = radio.receive(arr, len);
    for (i = 0; i < sizeof(value); i++){ //Read until struct is full
      *p++ = arr[i];
    }
    delete arr;
    return state;
}

template <class T> int SendAnythingLoRa(const T& value, unsigned int len){
    const byte* p = (const byte*)(const void*)&value;
    unsigned int i;
    
    for (i = 0; i < len; i++){
      const byte b = *p;
      transmitBuf[i] = b;
      p++;
    }
    int state = radio.transmit(transmitBuf, len);
    
    debug1 = len;
//    display.println();
//    for(int g = 0 ; g < len ; g++){
//      display.print(arr[g]);
//    }
    return state;
}
