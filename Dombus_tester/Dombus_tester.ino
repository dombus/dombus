#include <Wire.h>              //I2C väylä näytölle
#include <Adafruit_GFX.h>      //Grafiikkakirjasto
#include <Adafruit_SSD1306.h>  //Näytön ajurikirjasto
#include <OneWire.h>

const int buttonPin = PA0;

const int dallasPin = PB10;
const int blinkPin = PC13;
const int auxLedPin = PA1;
const int relay1Pin = PB5;
const int relay2Pin = PB4;

const int freq = 256;   //Soft pwm frequency in hz
const int steps = 256;  //How many steps in the pwm
float value = 10;

Adafruit_SSD1306 display(-1);
OneWire ds(dallasPin);  // on pin 10 (a 4.7K resistor is necessary)

void setup() {
  // put your setup code here, to run once:
  pinMode(blinkPin, OUTPUT);
  pinMode(auxLedPin, OUTPUT);
  pinMode(relay1Pin, OUTPUT);
  pinMode(relay2Pin, OUTPUT);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //Osoite on joko 0x3H tai 0x3C, usein valittavissa näytön takaa.
  display.setRotation(2);

  //rateaft = Wire1.i2c->currentRate;
  display.fillScreen(0);       //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
  display.setCursor(40, 20);   //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
  display.setTextColor(1, 0);  //Tekstin väri
  display.setTextSize(1);      //Tekstin koko, perusfontin koon moninkertana
  display.print("Lumisade!");  //Kirjoita näytölle jotakin!
}

void loop() {

  byte i;
  byte present = 0;
  byte type_s;
  byte data[9];
  byte addr[8];
  float celsius, fahrenheit;

  display.fillScreen(BLACK);
  display.setCursor(0, 0);

  if (!ds.search(addr)) {

    display.println("No more addresses.");
    display.println();
    ds.reset_search();
    delay(250);
    return;
  }

  display.print("ROM =");
  for (i = 0; i < 8; i++) {
    display.write(' ');
    display.print(addr[i], HEX);
  }

  if (OneWire::crc8(addr, 7) != addr[7]) {
    display.println("CRC is not valid!");
    return;
  }

  // the first ROM byte indicates which chip
  switch (addr[0]) {
    case 0x10:
      display.println("  Chip = DS18S20");  // or old DS1820
      type_s = 1;
      break;
    case 0x28:
      display.println("  Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
      display.println("  Chip = DS1822");
      type_s = 0;
      break;
    default:
      display.println("Device is not a DS18x20 family device.");
      return;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);  // start conversion, with parasite power on at the end

  delay(1000);  // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.

  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);  // Read Scratchpad

  //display.print("  Data = ");
  display.print(present, HEX);
  display.print(" ");
  for (i = 0; i < 9; i++) {  // we need 9 bytes
    data[i] = ds.read();
    display.print(data[i], HEX);
    display.print(" ");
  }
  //display.print(" CRC=");
  display.print(OneWire::crc8(data, 8), HEX);
  //display.println();

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3;  // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;       // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3;  // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1;  // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  display.print("t");
  display.print(celsius);
  display.print(" C, ");
  display.print(fahrenheit);
  display.println(" F");

  display.display();

  SoftPWM();
}

void SoftPWM() {
  static unsigned int stamp = 0;
  static unsigned int steppy = 0;
  static unsigned long stamp2 = 0;

  if (micros() - stamp > (1000000 / freq) / 256) {
    stamp = micros();


    if (steppy < (int)value) digitalWrite(blinkPin, HIGH);
    else digitalWrite(blinkPin, LOW);

    steppy++;

    if (steppy >= steps) {
      steppy = 0;
      float sinner = sin((float)millis() / 400);
      value = (sinner + 1.0) * 255.0;
      digitalWrite(auxLedPin, value < 127);
      digitalWrite(relay1Pin, value < 127);
      digitalWrite(relay2Pin, value < 30);
    }
  }

  if (millis() - stamp2 > 500) {
    //display.fillScreen(BLACK);
    // display.setCursor(0,0);
    // display.println(analogRead(buttonPin));
    // display.print("asd");

    // display.display();
    stamp2 = millis();
  }
}
