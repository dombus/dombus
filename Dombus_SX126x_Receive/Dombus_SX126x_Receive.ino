/*
   RadioLib SX126x Receive Example

   This example listens for LoRa transmissions using SX126x Lora modules.
   To successfully receive data, the following settings have to be the same
   on both transmitter and receiver:
    - carrier frequency
    - bandwidth
    - spreading factor
    - coding rate
    - sync word
    - preamble length

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/
#include <Wire.h>              //I2C väylä näytölle
#include <Adafruit_GFX.h>      //Grafiikkakirjasto
#include <Adafruit_SSD1306.h>  //Näytön ajurikirjasto

Adafruit_SSD1306 display(-1);

// include the library
#include <RadioLib.h>

int globalLine = 0;
int globalCol = 0;

// SX1262 has the following connections:
// NSS pin:   PB3
// DIO1 pin:  PB2
// NRST pin: PA15
// BUSY pin:  PA4
LLCC68 radio = new Module(PB3, PB2, PA15, PA4);

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

// or using CubeCell
//SX1262 radio = new Module(RADIOLIB_BUILTIN_MODULE);

void setup() {
  Wire.begin();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //Osoite on joko 0x3H tai 0x3C, usein valittavissa näytön takaa.
  display.setRotation(2);

  display.fillScreen(0);       //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
  display.setCursor(0, 0);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
  display.setTextColor(1, 0);  //Tekstin väri
  display.setTextSize(1);      //Tekstin koko, perusfontin koon moninkertana

  // locate devices on the bus
  display.print("Starting ");

  display.display();

  // initialize SX1262 with default settings
  Pront(F("Init.."));
  //int state = radio.begin();
  int state = radio.begin(434.0, 125.0, 9, 7,RADIOLIB_SX126X_SYNC_WORD_PRIVATE, 0, 8, 0.0, false);
  if (state == RADIOLIB_ERR_NONE) {
    Pront(F("success!"));
  } else {
    Pront(F("failed, code "));
    Pront(state);
    while (true);
  }

  radio.setRfSwitchPins(PB14, PB15);
}

void loop() {
  Pront(F("Wait.. "));

  // you can receive data as an Arduino String
  // NOTE: receive() is a blocking method!
  //       See example ReceiveInterrupt for details
  //       on non-blocking reception method.
  String str;
  int state = radio.receive(str);

  // you can also receive data as byte array
  /*
    byte byteArr[8];
    int state = radio.receive(byteArr, 8);
  */

  if (state == RADIOLIB_ERR_NONE) {
    // packet was successfully received
    Pront(F("success!"));

    // print the data of the packet
    Pront(F("Data: "));
    Pront(str);

    // print the RSSI (Received Signal Strength Indicator)
    // of the last received packet
    Pront(F("RSSI:"));
    Pront(radio.getRSSI());

    // print the SNR (Signal-to-Noise Ratio)
    // of the last received packet
    Pront(F("SNR: "));
    Pront(radio.getSNR());

  } else if (state == RADIOLIB_ERR_RX_TIMEOUT) {
    // timeout occurred while waiting for a packet
    Pront(F("timeout!"));

  } else if (state == RADIOLIB_ERR_CRC_MISMATCH) {
    // packet was received, but is malformed
    Pront(F("CRC error!"));

  } else {
    // some other error occurred
    Pront(F("fail, c "));
    Pront(state);

  }
}

template <class T>
void Pront(T thing) {
  display.setCursor(globalCol * 64, globalLine * 8);
  display.fillRect(globalCol * 64 , globalLine * 8, 64, 8, 0);
  display.print(thing);
  display.drawPixel(globalCol * 64 +62, globalLine * 8 -4, 1);
  display.display();
  //delay(500);
  globalLine++;
  if (globalLine > 3) {
    globalLine = 0;
    globalCol ++;
  }
  if (globalCol > 1) {
    globalLine = 0;
    globalCol = 0;
  }
}
