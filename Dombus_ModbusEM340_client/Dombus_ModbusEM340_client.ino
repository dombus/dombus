#include <ArduinoRS485.h> // ArduinoModbus depends on the ArduinoRS485 library
#include <ArduinoModbus.h>

#include <Wire.h>              //I2C väylä näytölle
#include <Adafruit_GFX.h>      //Grafiikkakirjasto
#include <Adafruit_SSD1306.h>  //Näytön ajurikirjasto

Adafruit_SSD1306 display(-1);
int globalLine = 0;
int globalCol = 0;

// == Inputs ==
const int buttonADCPin = PA0;
const int dallasPin = PB10;
const int fan1TachoPin = PB12;
const int fan2TachoPin = PB13;

const int Opto1Pin = PC15;
const int Opto2Pin = PC14;

// == Comms ==
const int LoRa_RXEN = PB14;
const int LoRa_TXEN = PB15;
const int LoRa_DIO1 = PB2;
const int LoRa_DIO2 = PA11;
const int LoRa_NRST = PA15;
const int LoRa_BUSY = PA4;
const int LoRa_NSS = PB3;
const int LoRa_MOSI = PA7; //"SPI1"
const int LoRa_MISO = PA6; //"SPI1"
const int LoRa_SCK = PA5;  //"SPI1"

const int RS485_RE_DE = PA8;
const int RS485_RX = PA9; //"Serial 1"
const int RS485_TX = PA10; //"Serial 1"
const int RX2Pin = PA3;  //"Serial 2"
const int TX2Pin = PA2;  //"Serial 2"

// == Outputs ==
const int fan1PWMPin = PB9;
const int fan2PWMPin = PB8;
const int ledPin = PC13;
const int relay1Pin = PB5;
const int relay2Pin = PB4;
const int AUX1Pin = PB1;
const int AUX2Pin = PB0;
const int AUX3PinLed = PA1;
const int WS2813_data = PA12;

const int fan1PWMChannel = 4;
const int fan2PWMChannel = 3;


int serverId = 1;
int regAddress = 52;


void setup() {
  Wire.begin();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //Osoite on joko 0x3H tai 0x3C, usein valittavissa näytön takaa.
  display.setRotation(0);

  display.fillScreen(0);       //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
  display.setCursor(0, 0);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
  display.setTextColor(1, 0);  //Tekstin väri
  display.setTextSize(1);      //Tekstin koko, perusfontin koon moninkertana

  // locate devices on the bus
  display.print("Starting ");

  display.display();
  
   // configure the LED
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  //Serial.begin(9600);
  //while (!Serial);
  pinMode(RS485_RE_DE, OUTPUT);
  //digitalWrite(RS485_RE_DE, HIGH);

  //Serial.println("Modbus RTU Client Toggle");
  RS485.setSerial(&Serial1);
  RS485.setPins(-1,RS485_RE_DE,RS485_RE_DE);
  RS485.begin(9600);
  // start the Modbus RTU client
  if (!ModbusRTUClient.begin(RS485,9600)) {
    Serial.println("Fail srtu");
    while (1){
      digitalWrite(ledPin, !digitalRead(ledPin));
      delay(100);
    }
  }
}

void loop() {
  // for (slave) id 1: write the value of 0x01, to the coil at address 0x00 
  if (!ModbusRTUClient.coilWrite(serverId, 0x00, 0x01)) {
    Pront("Failed wc! ");
    Pront(ModbusRTUClient.lastError());
    digitalWrite(ledPin, !digitalRead(ledPin));
  }

  // wait for 1 second
  delay(100);

  // for (slave) id 1: write the value of 0x00, to the coil at address 0x00 
  if (!ModbusRTUClient.coilWrite(serverId, 0x00, 0x00)) {
    Pront("Failed wc! ");
    Pront(ModbusRTUClient.lastError());
  }
  delay(100);

  // for (slave) id 1: write the value of 0x00, to the coil at address 0x00 
  long regval = ModbusRTUClient.inputRegisterRead(serverId,regAddress);
  if (regval<0) {
    Pront("Failed rr! ");
    Pront(ModbusRTUClient.lastError());
  }
  else{
    //Serial.println(regval);
    Pront(regval);
  }

  // wait for 1 second
  delay(500);
}

template <class T>
void Pront(T thing) {
  display.setCursor(globalCol * 64, globalLine * 8);
  display.fillRect(globalCol * 64 , globalLine * 8, 64, 8, 0);
  display.print(thing);
  display.drawPixel(globalCol * 64 +62, globalLine * 8 -4, 1);
  display.display();
  //delay(500);
  globalLine++;
  if (globalLine > 3) {
    globalLine = 0;
    globalCol ++;
  }
  if (globalCol > 1) {
    globalLine = 0;
    globalCol = 0;
  }
}
