/*
   RadioLib SX126x Receive Example

   This example listens for LoRa transmissions using SX126x Lora modules.
   To successfully receive data, the following settings have to be the same
   on both transmitter and receiver:
    - carrier frequency
    - bandwidth
    - spreading factor
    - coding rate
    - sync word
    - preamble length

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/
#include <Wire.h>              //I2C väylä näytölle
#include <Adafruit_GFX.h>      //Grafiikkakirjasto
#include <Adafruit_SSD1306.h>  //Näytön ajurikirjasto

const int blinkPin = PC13;
const int freq = 256;  //Soft pwm frequency in hz
const int steps = 256; //How many steps in the pwm
float value = 10;


Adafruit_SSD1306 display(-1);

// include the library
#include <RadioLib.h>

int globalLine = 0;
int globalCol = 0;

// flag to indicate that a packet was received
volatile bool receivedFlag = false;

// SX1262 has the following connections:
// NSS pin:   PB3
// DIO1 pin:  PB2
// NRST pin: PA15
// BUSY pin:  PA4
LLCC68 radio = new Module(PB3, PB2, PA15, PA4);

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

// or using CubeCell
//SX1262 radio = new Module(RADIOLIB_BUILTIN_MODULE);

void setup() {
  pinMode(blinkPin, OUTPUT);
  
  Wire.begin();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //Osoite on joko 0x3H tai 0x3C, usein valittavissa näytön takaa.
  display.setRotation(2);

  display.fillScreen(0);       //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
  display.setCursor(0, 0);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
  display.setTextColor(1, 0);  //Tekstin väri
  display.setTextSize(1);      //Tekstin koko, perusfontin koon moninkertana

  // locate devices on the bus
  display.print("Starting ");

  display.display();

  // initialize SX1262 with default settings
  Pront(F("Init.."));
  //int state = radio.begin();
  int state = radio.begin(434.0, 250.0, 6, 7, RADIOLIB_SX126X_SYNC_WORD_PRIVATE, 10, 8, 0.0, false);
  if (state == RADIOLIB_ERR_NONE) {
    Pront(F("success!"));
  } else {
    Pront(F("failed, code "));
    Pront(state);
    while (true);
  }

  radio.setDio1Action(setFlag);
  radio.setRfSwitchPins(PB14, PB15);

  // start listening for LoRa packets
  Pront(F("Listen.. "));
  state = radio.startReceive();
  if (state == RADIOLIB_ERR_NONE) {
    Pront(F("Lsuccess!"));
  } else {
    Pront(F("fLailed "));
    Pront(state);
    while (true);
  }
}



void loop() {
  if (receivedFlag) {
    receivedFlag = false;

    // you can receive data as an Arduino String
    // NOTE: receive() is a blocking method!
    //       See example ReceiveInterrupt for details
    //       on non-blocking reception method.
    String str;
    int state = radio.readData(str);

    // you can also receive data as byte array
    /*
      byte byteArr[8];
      int state = radio.receive(byteArr, 8);
    */

    if (state == RADIOLIB_ERR_NONE) {
      // packet was successfully received
      Pront(F("Got!"));

      // print the data of the packet
      Pront(F("Data: "));
      Pront(str);

      // print the RSSI (Received Signal Strength Indicator)
      // of the last received packet
      Pront(F("RSSI:"));
      Pront(radio.getRSSI());

      // print the SNR (Signal-to-Noise Ratio)
      // of the last received packet
      Pront(F("SNR: "));
      Pront(radio.getSNR());

    } else if (state == RADIOLIB_ERR_RX_TIMEOUT) {
      // timeout occurred while waiting for a packet
      Pront(F("timeout!"));

    } else if (state == RADIOLIB_ERR_CRC_MISMATCH) {
      // packet was received, but is malformed
      Pront(F("CRC error!"));

    } else {
      // some other error occurred
      Pront(F("fail, c "));
      Pront(state);

    }
    radio.startReceive();
  }
  SoftPWM();
}

void setFlag(void) {
  // we got a packet, set the flag
  receivedFlag = true;
}

template <class T>
void Pront(T thing) {
  display.setCursor(globalCol * 64, globalLine * 8);
  display.fillRect(globalCol * 64 , globalLine * 8, 64, 8, 0);
  display.print(thing);
  display.drawPixel(globalCol * 64 + 62, globalLine * 8 - 4, 1);
  display.display();
  //delay(500);
  globalLine++;
  if (globalLine > 3) {
    globalLine = 0;
    globalCol ++;
  }
  if (globalCol > 1) {
    globalLine = 0;
    globalCol = 0;
  }
}

void SoftPWM() {
  static unsigned int stamp = 0;
  static unsigned int steppy = 0;

  if (micros() - stamp > (1000000 / freq) / 256) {
    stamp = micros();


    if(steppy < (int)value) digitalWrite(blinkPin,HIGH);
    else digitalWrite(blinkPin,LOW);

    steppy++;

    if(steppy >= steps){
      steppy = 0;
      float sinner = sin((float)millis()/400);
      value = (sinner+1.0)*255.0;
    }

  }
}
