#include "JoyHandler.h"

State::State(){
  Reset();
}

void State::Reset(){
  state = false;
  repeatArmed = false;
  repeating = false;
  repeatTimer = 0;
}

JoyHandler::JoyHandler(uint32_t repeatDelay, uint32_t repeatMs) {
  _timer = _repeatTimer = 0;
  _event = false;
  up = State();
  down = State();
  left = State();
  right = State();
  pressed = State();
  released = State();
  _repeatDelay = repeatDelay;
  _repeatMs = repeatMs;
}

void JoyHandler::Update(int buttonADC) {
  _timer = millis();
  up.state = down.state = left.state = right.state = pressed.state = released.state = false;
  switch (buttonADC) {  //Joy press
    case 300 ... 500:
      //if (btnPressed) return;
      //currentButton = ARM;
      pressed.state = true;    //Clear this flag
      break;

    // case 501 ... 800:   //joy left
    //   //if (btnPressed) return;
    //   //currentButton = CALIB;
    //   left.state = true;
    //   break;

    case 501 ... 800:   //joy up
      //if (btnPressed) return;
      //currentButton = CALIB;
      up.state = true;
      break;

    // case 0 ... 100:     //Joy right
    //   //if (btnPressed) return;
    //   //currentButton = CALIB;
    //   right.state = true;
    //   break;

    case 0 ... 100:     //Joy down
      //if (btnPressed) return;
      //currentButton = CALIB;
      down.state = true;
      break;

    default:
      //currentButton = NONE;
      _event = false;
      released.state = true;
      break;
  }
}

void JoyHandler::SetRepeatMs(uint32_t repeatMs){
  _repeatMs = repeatMs;
}

bool JoyHandler::Up(){
  return up.state;
}
bool JoyHandler::Down(){
  return down.state;
}
bool JoyHandler::Left(){
  return left.state;
}
bool JoyHandler::Right(){
  return right.state;
}
bool JoyHandler::Pressed(){
  return pressed.state;
}
bool JoyHandler::Released(){
  return released.state;
}

bool JoyHandler::UpR(){
  return _Repeater(up);
}
bool JoyHandler::DownR(){
  return _Repeater(down);
}
bool JoyHandler::LeftR(){
  return _Repeater(left);
  return true;
}
bool JoyHandler::RightR(){
  return _Repeater(right);
}
bool JoyHandler::PressedR(){
  return _Repeater(pressed);
}

bool JoyHandler::UpEvent(){
  if(up.state && !_event){
    _event = true;
    return true;
  }
  else return false;
}
bool JoyHandler::DownEvent(){
  if(down.state && !_event){
    _event = true;
    return true;
  }
  else return false;
}
bool JoyHandler::LeftEvent(){
  if(left.state && !_event){
    _event = true;
    return true;
  }
  else return false;
}
bool JoyHandler::RightEvent(){
  if(right.state && !_event){
    _event = true;
    return true;
  }
  else return false;
}
bool JoyHandler::PressedEvent(){
  if(pressed.state && !_event){
    _event = true;
    return true;
  }
  else return false;
}

bool JoyHandler::_Repeater(State &input){
  bool toReturn = false; //By default, return false;
  
  if(!input.repeatArmed){  //If this is first time in a new repeat (since joystick released), use repeat timer to enter actual repeating.
    input.repeatTimer = _timer;
  }
  
  if(input.state){ //If joystick is now (still) in the position
    if(input.repeating){ //If we are repeating
      if(_timer-input.repeatTimer > _repeatMs){  //if it is time to repeat again
        input.repeatTimer = _timer;
        toReturn = true;
      }
    }
    else{ //If not repeating
      if(!input.repeatArmed){ //If repeatig not yet armed, do that and return true this time
        toReturn = true;
        input.repeatArmed = true;
      }
      if(_timer-input.repeatTimer > _repeatDelay){ //If  enough time has passed to start repeating, return true and repeat on subsequent calls until joystick released
        input.repeating = true;
        input.repeatTimer = _timer;
        toReturn = true;
      }
    }
  }
  else{  //If joystick is released, reset all repeating state state
    input.repeating = false;
    input.repeatArmed = false;
  }

  return toReturn;
}
