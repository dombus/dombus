#include <arduino.h>

#define TORAD 0.017453293

byte wide = 96/2;    //Indicator dial width;

struct pixcoord {
  unsigned int x;
  unsigned int y;
};

struct indicator {
  unsigned int x;
  unsigned int y;
  int value;
  int oldAngle;
};
struct data {
  byte what;  //What piece of date this is, denoted by a number from 0-255
  int payload;
};

struct DateStore {
  uint16_t year;
  uint8_t month;
  uint8_t day;
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
  bool enabled;
};

char Sign(int x) {  //Simply returns sign of input (-1 or 1)
  if (x < 0) return -1;
  else return 1;
}


unsigned int FloatToUint(float input) {  // Converts a negative or positive to a int.
  if (input > 65535) {
    return 65535;
  } else if (input < 0) {
    return 0;
  }
  input > 0 ? (unsigned int)floor(input + 0.5) : (unsigned int)ceil(input - 0.5);
  return input;
}

int FloatToInt2(float input) {  // Converts a negative or positive to a int.
  if (input > 32767) {
    return 32767;
  } else if (input < -32768) {
    return -32768;
  }
  input > 0 ? (int)floor(input + 0.5) : (int)ceil(input - 0.5);
  return input;
}