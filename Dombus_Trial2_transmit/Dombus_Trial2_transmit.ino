/*
   RadioLib SX126x Transmit Example

   This example transmits packets using SX1262 LoRa radio module.
   Each packet contains up to 256 bytes of data, in the form of:
    - Arduino String
    - null-terminated char array (C-string)
    - arbitrary binary data (byte array)

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/
#include <Dombus.h>
#include <Wire.h>              //I2C väylä näytölle
#include <Adafruit_GFX.h>      //Grafiikkakirjasto
#include <Adafruit_SSD1306.h>  //Näytön ajurikirjasto
#include <ArduinoRS485.h>      // ArduinoModbus depends on the ArduinoRS485 library
#include <ArduinoModbus.h>
#include <RadioLib.h>

#include <STM32RTC.h>

#include "JoyHandler.h"

//=========================================================================================================================

int serverId = 7;
int regAddress = 0x28;
int regAddress2 = 0x36;

int globalLine = 0;
int globalCol = 0;

// == Inputs ==
const int buttonADCPin = PA0;
const int dallasPin = PB10;
const int fan1TachoPin = PB12;
const int fan2TachoPin = PB13;

const int Opto1Pin = PC15;
const int Opto2Pin = PC14;

// == Comms ==
const int LoRa_RXEN = PB14;
const int LoRa_TXEN = PB15;
const int LoRa_DIO1 = PB2;
const int LoRa_DIO2 = PA11;
const int LoRa_NRST = PA15;
const int LoRa_BUSY = PA4;
const int LoRa_NSS = PB3;
const int LoRa_MOSI = PA7;  //"SPI1"
const int LoRa_MISO = PA6;  //"SPI1"
const int LoRa_SCK = PA5;   //"SPI1"

const int RS485_RE_DE = PA8;
const int RS485_RX = PA9;   //"Serial 1"
const int RS485_TX = PA10;  //"Serial 1"
const int RX2Pin = PA3;     //"Serial 2"
const int TX2Pin = PA2;     //"Serial 2"

// == Outputs ==
const int fan1PWMPin = PB9;
const int fan2PWMPin = PB8;
const int ledPin = PC13;
const int relay1Pin = PB5;
const int relay2Pin = PB4;
const int AUX1Pin = PB1;
const int AUX2Pin = PB0;
const int AUX3PinLed = PA1;
const int WS2813_data = PA12;

const int fan1PWMChannel = 4;
const int fan2PWMChannel = 3;

/* Change these values to set the current initial time */
const byte seconds = 0;
const byte minutes = 0;
const byte hours = 16;

/* Change these values to set the current initial date */
/* Monday 15th June 2015 */
const byte weekDay = 1;
const byte day = 12;
const byte month = 11;
const byte year = 23;

//=========================================================================================================================

/* Get the rtc object */
STM32RTC& rtc = STM32RTC::getInstance();

pixcoord throttleDial = { 0, 0 };                             //Where the dials are located, x/y
indicator Throttle = { throttleDial.x, throttleDial.y, 0, 0 };  //Indicator = location x, location y, value to use, previous value to erase.


void TimeMenu(char thing);

typedef void (*FunctionPointer)(char pressedButton);  //Typedef for a function pointer pointing to currently active UI screen.
FunctionPointer CurrentUiFunction = TimeMenu;         //Start with showing the main menu

// SX1262 has the following connections:
// NSS pin:   PB3
// DIO1 pin:  PB2
// NRST pin: PA15
// BUSY pin:  PA4
int debug1 = 0;

//SPIClass SPI_1(PA7, PA6, PA5);
//SPISettings spiSettings(2000000, MSBFIRST, SPI_MODE0);
//SX1278 radio = new Module(cs, irq, rst, gpio, spi, spiSettings);

LLCC68 radio = new Module(LoRa_NSS, LoRa_DIO1, LoRa_NRST, LoRa_BUSY);
//LLCC68 radio = new Module(PB3, PB2, PA15, PA4, SPI_1, spiSettings);

Adafruit_SSD1306 display(-1);
HardwareTimer* MyTim = new HardwareTimer(TIM4);  // TIM3 is MCU hardware peripheral instance, its definition is provided in CMSIS
JoyHandler Joystick(300, 50);                    //Controls joystick. (repeat start delay, repeat delay)

struct __attribute__((packed, aligned(4))) EM340_struct {
  int32_t V_L1;
  int32_t V_L2;
  int32_t V_L3;
  int32_t A_L1;
};

EM340_struct EM340 = { 12000, 555, 12, 53 };
byte transmitBuf[16] = { 0 };

DateStore startTime1 = {0,0,0,20,0,0,true};
DateStore stopTime1 = {0,0,0,8,0,0,false};

DateStore startTime2 = {0,0,0,6,0,0,false};
DateStore stopTime2 = {0,0,0,1,0,0,false};

//=========================================================================================================================


void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);

  pinMode(RS485_RE_DE, OUTPUT);
  digitalWrite(RS485_RE_DE, HIGH);

  pinMode(relay1Pin,OUTPUT);

  rtc.begin();
  rtc.setTime(hours, minutes, seconds);
  rtc.setDate(weekDay, day, month, year);

  //Serial.println("Modbus RTU Client Toggle");
  RS485.setSerial(&Serial1);
  RS485.setPins(-1, RS485_RE_DE, RS485_RE_DE);
  RS485.begin(9600);
  // start the Modbus RTU client
  if (!ModbusRTUClient.begin(RS485, 9600)) {
    Pront("Fail srtu");
    while (1) {
      digitalWrite(ledPin, !digitalRead(ledPin));
      delay(100);
    }
  } else {
    Pront("485 st");
  }



  Wire.begin();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //Osoite on joko 0x3H tai 0x3C, usein valittavissa näytön takaa.
  display.setRotation(2);

  display.fillScreen(0);       //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
  display.setCursor(0, 0);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
  display.setTextColor(1, 0);  //Tekstin väri
  display.setTextSize(1);      //Tekstin koko, perusfontin koon moninkertana

  // locate devices on the bus
  display.print("Starting ");

  display.display();


  //SPI_1.begin();

  // initialize SX1262 with default settings
  Pront(F("init"));
  //freq = 434.0,bw = 125.0, sf = 9, cr = 7, syncWord = RADIOLIB_SX126X_SYNC_WORD_PRIVATE, power = 10, preambleLength = 8, tcxoVoltage = 1.6, useRegulatorLDO = false);
  radio.autoLDRO();
  int state = radio.begin(434.0, 125.0, 9, 7, RADIOLIB_SX126X_SYNC_WORD_PRIVATE, 0, 8, 0.0, false);
  if (state == RADIOLIB_ERR_NONE) {
    Pront(F("success!"));
  } else {
    Pront(F("failed, "));
    Pront(state);
    while (true)
      ;
  }

  // some modules have an external RF switch
  // controlled via two pins (RX enable, TX enable)
  // to enable automatic control of the switch,
  // call the following method
  // RX enable:   PB14
  // TX enable:   PB15

  radio.setRfSwitchPins(LoRa_RXEN, LoRa_TXEN);
  //radio.setOutputPower(-9);
}

void loop() {
  AnalogHandler();
  //ReadModbusSensors();
  //TransmitViaRadio();
  UiFunction();
  CheckOutputs();
}

template<class T>
void Pront(T thing) {
  display.setCursor(globalCol * 64, globalLine * 8);
  display.fillRect(globalCol * 64, globalLine * 8, 64, 8, 0);
  display.print(thing);
  display.drawPixel(globalCol * 64 + 62, globalLine * 8 - 4, 1);
  display.display();
  globalLine++;
  if (globalLine > 3) {
    globalLine = 0;
    globalCol++;
  }
  if (globalCol > 1) {
    globalLine = 0;
    globalCol = 0;
  }
}

template<class T> int GetAnything(const T& value, unsigned int len) {
  byte* p = (byte*)(void*)&value;
  byte* arr = new byte(len);
  unsigned int i;

  int state = radio.receive(arr, len);
  for (i = 0; i < sizeof(value); i++) {  //Read until struct is full
    *p++ = arr[i];
  }
  delete arr;
  return state;
}

template<class T> int SendAnythingLoRa(const T& value, unsigned int len) {
  const byte* p = (const byte*)(const void*)&value;
  unsigned int i;

  for (i = 0; i < len; i++) {
    const byte b = *p;
    transmitBuf[i] = b;
    p++;
  }
  int state = radio.transmit(transmitBuf, len);

  debug1 = len;
  //    display.println();
  //    for(int g = 0 ; g < len ; g++){
  //      display.print(arr[g]);
  //    }
  return state;
}

void AnalogHandler() {
  static unsigned int anaStamp = 0;

  if (millis() - anaStamp > 10) {
    Joystick.Update(analogRead(buttonADCPin));
    anaStamp = millis();
  }
}

void ReadModbusSensors() {
  static unsigned long timerino = 0;
  static byte whichSensor = 0;

  if (millis() - timerino > 1000) {
    timerino = millis();

    if (whichSensor == 0) {
      int retcval = ModbusRTUClient.requestFrom(serverId, INPUT_REGISTERS, 0x0000, 6);
      //long retcval = ModbusRTUClient.inputRegisterRead(serverId,regAddress);
      if (retcval == -1) {
        //Pront("485 fr");
      } else {
        //Pront("volts "); //Pront(retcval);
        int i = 0;
        uint32_t arr[6] = { 0 };
        while (ModbusRTUClient.available()) {
          arr[i] = ModbusRTUClient.read();
          i++;
        }
        EM340.V_L1 = (int32_t)((uint32_t)arr[1] << 16 | (uint32_t)arr[0] << 0);
        EM340.V_L2 = (int32_t)((uint32_t)arr[3] << 16 | (uint32_t)arr[2] << 0);
        EM340.V_L3 = (int32_t)((uint32_t)arr[5] << 16 | (uint32_t)arr[4] << 0);
        //Pront(EM340.V_L1);
        //Pront(EM340.V_L2);
        //Pront(EM340.V_L3);
      }
    }

    else if (whichSensor == 1) {
      int retcval2 = ModbusRTUClient.requestFrom(serverId, INPUT_REGISTERS, 0x0028, 2);
      //long retcval = ModbusRTUClient.inputRegisterRead(serverId,regAddress);
      if (retcval2 == -1) {
        //Pront("485 fa");
        //Pront(ModbusRTUClient.lastError());
      } else {
        //Pront("watts "); //Pront(retcval);
        //Pront("watts ");
        int i = 0;
        uint32_t arr[2] = { 0 };
        while (ModbusRTUClient.available()) {
          arr[i] = ModbusRTUClient.read();
          i++;
        }
        EM340.A_L1 = (int32_t)((uint32_t)arr[1] << 16 | (uint32_t)arr[0] << 0);
        //Pront(EM340.A_L1);
      }
    }

    if (whichSensor >= 1) {
      whichSensor++;
    } else {
      whichSensor = 0;
    }
  }
}
void TransmitViaRadio() {
  static unsigned long taimeri = 0;

  // wait for a second before transmitting again
  if (millis() - taimeri >= 1000) {
    taimeri = millis();
    // you can transmit C-string or Arduino string up to
    // 256 characters long
    // NOTE: transmit() is a blocking method!
    //       See example SX126x_Transmit_Interrupt for details
    //       on non-blocking transmission method.

    int state = SendAnythingLoRa(EM340, sizeof(EM340_struct));

    // you can also transmit byte array up to 256 bytes long
    /*
      byte byteArr[] = {0x01, 0x23, 0x45, 0x56, 0x78, 0xAB, 0xCD, 0xEF};
      int state = radio.transmit(byteArr, 8);
    */

    if (state == RADIOLIB_ERR_NONE) {
      // the packet was successfully transmitted
      //Pront(F("success!"));

      // print measured data rate
      //Pront(F("Datarate:"));
      //Pront(radio.getDataRate());
      //Pront(debug1);

    } else if (state == RADIOLIB_ERR_PACKET_TOO_LONG) {
      // the supplied packet was longer than 256 bytes
      //Pront(F("too long!"));

    } else if (state == RADIOLIB_ERR_TX_TIMEOUT) {
      // timeout occured while transmitting packet
      //Pront(F("timeout!"));


    } else {
      // some other error occurred
      //Pront(F("failed, "));
      //Pront(state);
    }
  }
}

void DrawDial(pixcoord loc, String title) {  //Draws a fixed size dial outline (memory constraints..) Replace with bmp?
  //byte wide = 96/2;      //width of dial, heigth is half of that. Adjust globally.
  float sweepdeg = 0;
  float radius = wide / 0.707107;
  display.drawRect(loc.x, loc.y, 2 * wide + 1, wide + 1, WHITE);  //17266

  for (sweepdeg; sweepdeg < 181; sweepdeg += 15) {
    DrawBoxLimitedLine(loc.x, loc.y, sweepdeg, (float)wide, WHITE);
  }
  display.fillRect(loc.x + 5, loc.y, 2 * wide - 9, wide - 5, BLACK);
  display.setCursor(loc.x, loc.y - 9);
  display.print(F("min"));
  display.setCursor(loc.x + wide - 8, loc.y - 9);
  display.print(title);
  display.setCursor(loc.x + 2 * wide - 15, loc.y - 9);
  display.print(F("max"));
}

void DrawBoxLimitedLine(unsigned int x, unsigned int y, float sweepdeg, float coreradius, unsigned int colour) {
  float tempradius;  //Radius for current spoke that reaches to the bounding box.
  if ((sweepdeg > 45 && sweepdeg < 135) || (sweepdeg > 225 && sweepdeg < 315)) {
    tempradius = abs(coreradius / sin(TORAD * sweepdeg));
  } else {
    tempradius = abs(coreradius / cos(TORAD * sweepdeg));
  }
  display.drawLine(x + wide, y, (x + wide) + FloatToInt2(cos(TORAD * sweepdeg) * tempradius), (y) + FloatToInt2(sin(TORAD * sweepdeg) * tempradius), colour);
}

void DrawIndicator(indicator* me) {  //Draws the dial's indicator needle using it's coordinates. Shows current numerical value in the base. This is not static (obviously)

  byte printLen = 1;
  int value = me->value;

  if (value < 10) printLen = 1;
  else if (value < 100 && value > -100) printLen = 2;
  else printLen = 3;
  unsigned int x = me->x;
  unsigned int y = me->y;

  unsigned int curx = x + wide - printLen / 2 * 6 - printLen % 2 * 3;
  if (value > -181 && value < 181) {
    //DrawBoxLimitedLine(x,y, (float)(180-me->oldAngle), 39, BLACK);
    DrawBoxLimitedLine(x, y, (float)(180 - value), 39, WHITE);
    me->oldAngle = value;
  }



  //display.fillRect(x+wide-9,y,18,8,BLACK);
  display.setCursor(curx, y);
  display.print(value);
}

void UiFunction() {
  static long timermaid = 0;
  if (millis() - timermaid > 50) {
    timermaid = millis();
    CurrentUiFunction('a');
  }
}

void HomeMenu(char thing) {
  Throttle.value = millis()/1000;
  DrawDial(throttleDial,"thr");
  DrawIndicator(&Throttle);
  display.display();
  if (Joystick.PressedEvent()) {
    CurrentUiFunction = TimeMenu;
  }
}
void TimeMenu(char thing) {
  const unsigned long selectTime = 200;
  const unsigned long editTime = 100;
  static unsigned long blinkTime = selectTime;
  const uint8_t maxSelector = 9;
  static unsigned long blinkerTimer = 0;
  static bool showBlinking = false;
  static uint8_t selector = 0;
  static int8_t selector2 = 0;
  static bool editing = false;
  static bool editing2 = false;
  static int editor = 0;
  //static 

  display.clearDisplay();

  
  
  //Draw dotted lines to delimit minute and hour bars
  for(int x = 0; x < 128 ; x += 7){
    display.drawFastHLine(x, 8, 3, WHITE);
    display.drawFastHLine(x, 14, 3, WHITE);
    display.drawFastHLine(x, 20, 3, WHITE);
  }
  //Draw hour indicator bar
  display.fillRect(map(rtc.getHours(),0,24,0,128)-1, 9, 3, 5, WHITE);

  //Draw minute indicator bar
  display.fillRect(map(rtc.getMinutes(),0,60,0,128)-1, 15, 3, 5, WHITE);

  //Draw second indicator bar
  display.drawFastHLine(0, 21, map(rtc.getSeconds(),0,60,0,128), WHITE);


  //Draw the active hours indicator
  for(int x = 0 ; x < 23 ; x++){
    if(!startTime1.enabled && !startTime2.enabled){ //Neither timer is enabled, never on
      //display.fillrect(0,10,128,)
    }
    else if(startTime1.enabled && startTime2.enabled){  //Both enabled

    }
    else if(startTime1.enabled){  //Only time 1 enabled
      if(startTime1.hour <= stopTime1.hour){ //Time does not wrap around
        int hoursToDraw = stopTime1.hour - startTime1.hour;
        display.fillRect(map(startTime1.hour,0,24,0,128), 10, map(hoursToDraw,0,24,0,128), 3, WHITE);
      }
      else{   //Time does wrap around so draw from start to "end", then from "beginning" to stop.
        int startpixel = map(startTime1.hour,0,24,0,128);
        int tostop = 127 - startpixel;
        display.fillRect(startpixel, 10, tostop, 3, WHITE);
        int stoppixel2 = map(stopTime1.hour,0,24,0,128);
        display.fillRect(0, 10, stoppixel2, 3, WHITE);
      }
    }
    else if(startTime2.enabled){  //Only time 2 enabled
      if(startTime2.hour <= stopTime2.hour){ //Time does not wrap around
        int hoursToDraw = stopTime2.hour - startTime2.hour;
        display.fillRect(map(startTime2.hour,0,24,0,128), 10, map(hoursToDraw,0,24,0,128), 3, WHITE);
      }
      else{   //Time does wrap around so draw from start to "end", then from "beginning" to stop.
        int startpixel = map(startTime2.hour,0,24,0,128);
        int tostop = 127 - startpixel;
        display.fillRect(startpixel, 10, tostop, 3, WHITE);
        int stoppixel2 = map(stopTime2.hour,0,24,0,128);
        display.fillRect(0, 10, stoppixel2, 3, WHITE);
      }
    }
  }

  if(!editing && Joystick.DownEvent()){
    selector++;
    if(selector > maxSelector){
      selector = 0;
    }
  }
  else if(!editing && Joystick.UpEvent()){
    selector--;
    if(selector > maxSelector){
      selector = maxSelector;
    }
  }



  if(millis() - blinkerTimer > blinkTime){
    blinkerTimer = millis();
    showBlinking = !showBlinking;
  }


  // Draw the current time on top except under selector
  display.setCursor(0,0);
  (selector == 1) ? display.print("  ") : display.printf("%02d", rtc.getDay());  display.print('/');
  (selector == 2) ? display.print("  ") : display.printf("%02d", rtc.getMonth());  display.print('/');
  (selector == 3) ? display.print("    ") : display.printf("%04d", rtc.getYear()+2000); 
  display.setCursor(64,0);
  (selector == 4) ? display.print("  ") : display.printf("%02d", rtc.getHours()); display.print(':');
  (selector == 5) ?  display.print("  ") : display.printf("%02d", rtc.getMinutes()); display.print(':');
  display.printf("%02d.%d",rtc.getSeconds(), rtc.getSubSeconds()/100);

  // Draw the start stop time indicators on bottom except under selector
  display.setCursor(0,24);
  (selector == 6) ? display.print("       : ") : display.print("Times 1: "); 
  (selector == 7) ? display.print(" ") : (startTime1.enabled) ? display.print('y') : display.print('n');
  display.setCursor(68,24);
  (selector == 8) ? display.print("       : ") : display.print("Times 2: "); 
  (selector == 9) ? display.print(" ") : (startTime2.enabled) ? display.print('y') : display.print('n');

  

  if(selector == 1 || selector == 2 ||selector == 3 ||selector == 4 ||selector == 5) {
    if(editing){
      int maximum;
      if(selector == 1) maximum = 31;        //Days
      else if (selector == 2) maximum = 12;  //Months
      else if (selector == 3) maximum = 99;  //Years
      else if (selector == 4) maximum = 24;  //Hours
      else if (selector == 5) maximum = 60;  //Minutes

      blinkTime = editTime;
      if(Joystick.DownR()){
        editor++;
        if(editor > maximum){
          editor = 0;
        }
      }
      if(Joystick.UpR()){
        editor--;
        if(editor < 0){
          editor = maximum;
        }
      }
      //(showBlinking) ? display.printf("%02d", editor) : display.print("  ");
      if(Joystick.PressedEvent()){
        editing = false;
        if(selector == 1) rtc.setDay(editor);
        else if (selector == 2) rtc.setMonth(editor);
        else if (selector == 3) rtc.setYear(editor);
        else if (selector == 4) rtc.setHours(editor);
        else if (selector == 5) rtc.setMinutes(editor);
      }
      
    }
    else{
        if(selector == 1) editor = rtc.getDay();
        else if (selector == 2) editor = rtc.getMonth();
        else if (selector == 3) editor = rtc.getYear();
        else if (selector == 4) editor = rtc.getHours();
        else if (selector == 5) editor = rtc.getMinutes();
      if(Joystick.PressedEvent()){
        editing = true;
      }
      blinkTime = selectTime;
      //(showBlinking) ? display.printf("%02d", rtc.getDay()) : display.print("  ");
    }
    if(selector == 1) {display.setCursor(0,0); (showBlinking) ? display.printf("%02d",editor) : display.print("  ");}
    else if (selector == 2) {display.setCursor(18,0); (showBlinking) ? display.printf("%02d",editor) : display.print("  ");}
    else if (selector == 3) {display.setCursor(36,0); (showBlinking) ? display.printf("%04d",editor+2000) : display.print("  ");}
    else if (selector == 4) {display.setCursor(64,0); (showBlinking) ? display.printf("%02d",editor) : display.print("  ");}
    else if (selector == 5) {display.setCursor(82,0); (showBlinking) ? display.printf("%02d",editor) : display.print("  ");}
  }
  //Deal with the time selectors
  else if (selector == 6 || selector == 7 || selector == 8 || selector == 9) {
    //If editing time selectors
    if(editing){
      //Clean the bottom row when editing to display start and stop times
      display.fillRect(0,24,128,8,BLACK);
      
      //Editing time 1
      if(selector == 6){
        
        if(Joystick.UpR()){
          if(editing2){  //Increment
              if(selector2 == 0){ 
                startTime1.hour--;
                if(startTime1.hour > 23) startTime1.hour = 23;
              }
              else if(selector2 == 1){
                startTime1.minute--;
                if(startTime1.minute > 59) startTime1.minute = 59;
              }
              else if(selector2 == 2){
                stopTime1.hour--;
                if(stopTime1.hour > 23) stopTime1.hour = 23;
              }
              else if(selector2 == 3){
                stopTime1.minute--;
                if(stopTime1.minute > 59) stopTime1.minute = 59;
              }
          }
          else{
              selector2--;
              if(selector2 < 0) selector2 = 4;
          }
          
        }
        else if(Joystick.DownR()){
          if(editing2){
              if(selector2 == 0){ 
                startTime1.hour++;
                if(startTime1.hour > 23) startTime1.hour = 0;
              }
              else if(selector2 == 1){
                startTime1.minute++;
                if(startTime1.minute > 59) startTime1.minute = 0;
              }
              else if(selector2 == 2){
                stopTime1.hour++;
                if(stopTime1.hour > 23) stopTime1.hour = 0;
              }
              else if(selector2 == 3){
                stopTime1.minute++;
                if(stopTime1.minute > 59) stopTime1.hour = 0;
              }
          }
          else{
              selector2++;
              if(selector2 > 4) selector2 = 0;
          }
        }

        //Draw bottom row while editing
        display.setCursor(0,24);
        display.printf("On ");
        (selector2 != 0) ? display.printf("%02d",startTime1.hour) : (showBlinking) ? display.printf("%02d",startTime1.hour) : display.print("  ");
        display.print(':');
        (selector2 != 1) ? display.printf("%02d",startTime1.minute) : (showBlinking) ? display.printf("%02d",startTime1.minute) : display.print("  ");
        display.setCursor(52, 24); display.print("Off ");
        (selector2 != 2) ? display.printf("%02d",stopTime1.hour) : (showBlinking) ? display.printf("%02d",stopTime1.hour) : display.print("  ");
        display.print(':');
        (selector2 != 3) ? display.printf("%02d",stopTime1.minute) : (showBlinking) ? display.printf("%02d",stopTime1.minute) : display.print("  ");
        display.setCursor(110,24);
        (selector2 != 4) ? display.print("Bck") : (showBlinking) ? display.print("Bck") : display.print("   ");
        
      }
      else if(selector == 8){
        if(Joystick.UpR()){
          if(editing2){  //Increment
              if(selector2 == 0){ 
                startTime2.hour--;
                if(startTime2.hour > 23) startTime2.hour = 23;
              }
              else if(selector2 == 1){
                startTime2.minute--;
                if(startTime2.minute > 59) startTime2.minute = 59;
              }
              else if(selector2 == 2){
                stopTime2.hour--;
                if(stopTime2.hour > 23) stopTime2.hour = 23;
              }
              else if(selector2 == 3){
                stopTime2.minute--;
                if(stopTime2.minute > 59) stopTime2.minute = 59;
              }
          }
          else{
              selector2--;
              if(selector2 < 0) selector2 = 4;
          }
          
        }
        else if(Joystick.DownR()){
          if(editing2){
              if(selector2 == 0){ 
                startTime2.hour++;
                if(startTime2.hour > 23) startTime2.hour = 0;
              }
              else if(selector2 == 1){
                startTime2.minute++;
                if(startTime2.minute > 59) startTime2.minute = 0;
              }
              else if(selector2 == 2){
                stopTime2.hour++;
                if(stopTime2.hour > 23) stopTime2.hour = 0;
              }
              else if(selector2 == 3){
                stopTime2.minute++;
                if(stopTime2.minute > 59) stopTime2.hour = 0;
              }
          }
          else{
              selector2++;
              if(selector2 > 4) selector2 = 0;
          }
        }

        //Draw bottom row while editing
        display.setCursor(0,24);
        display.printf("On ");
        (selector2 != 0) ? display.printf("%02d",startTime2.hour) : (showBlinking) ? display.printf("%02d",startTime2.hour) : display.print("  ");
        display.print(':');
        (selector2 != 1) ? display.printf("%02d",startTime2.minute) : (showBlinking) ? display.printf("%02d",startTime2.minute) : display.print("  ");
        display.setCursor(52, 24); display.print("Off ");
        (selector2 != 2) ? display.printf("%02d",stopTime2.hour) : (showBlinking) ? display.printf("%02d",stopTime2.hour) : display.print("  ");
        display.print(':');
        (selector2 != 3) ? display.printf("%02d",stopTime2.minute) : (showBlinking) ? display.printf("%02d",stopTime2.minute) : display.print("  ");
        display.setCursor(110,24);
        (selector2 != 4) ? display.print("Bck") : (showBlinking) ? display.print("Bck") : display.print("   ");
      }

      //Check time value bounds

      //Figure out what to do with press
      if(Joystick.PressedEvent()){
        if(editing2){  //Done editing a number
          editing2 = false;
          blinkTime = selectTime;
        }
        else{
          if(selector2 == 4){ //done editing altogether
            editing = false;
            selector2 = 0;
            blinkTime = selectTime;
          }
          else{
            editing2 = true;
            blinkTime = editTime;
          }
        }

      }
    }
    else{ //If not editing, wait for start of editing
      if(Joystick.PressedEvent()){
          if(selector == 7){
            startTime1.enabled = !startTime1.enabled;
          }
          else if(selector == 9){
            startTime2.enabled = !stopTime1.enabled;
          }
          else{
            editing = true;
            blinkTime = selectTime;
          }
      }
    }

    if(editing){

    }
    else{
        if(selector == 6 && !editing) {display.setCursor(0,24); (showBlinking) ? display.print("Timas 1: ") : display.print("         "); }
        else if(selector == 7) {display.setCursor(54,24); (!showBlinking) ? display.print(' ')  : (startTime1.enabled) ? display.print('y') : display.print('n'); }
        else if(selector == 8 && !editing) {display.setCursor(68,24); (showBlinking) ? display.print("Times 2: ") : display.print("         "); }
        else if(selector == 9) {display.setCursor(122,24); (!showBlinking) ? display.print(' ')  : (startTime2.enabled) ? display.print('y') : display.print('n'); }
    }

  }


  //display.printf("%02d:%02d", rtc.getHours(), rtc.getMinutes()); display.print(' ');
 // display.printf("%02d:%02d", rtc.getSeconds(), rtc.getMinutes());


  // if(selector == 1){
  //   display.setCursor(0,0);
  //   display.print('*');
  // }

  //Draw selector star
  // if(selector == 6){
  //   display.setCursor(0,0);
  //   display.print('*');
  // }
  // else if(selector == 7){
  //   display.setCursor(64,0);
  //   display.print('*');
  // }
  // else if(selector == 8){
  //   display.setCursor(0,24);
  //   display.print('*');
  // }
  // else if(selector == 3){
  //   display.setCursor(64,24);
  //   display.print('*');
  // }

  

  
  
  display.display();


  if (!editing && Joystick.PressedEvent()) {
    CurrentUiFunction = HomeMenu;
  }
}

//Find how many minutes backward from current time another time is (wrapping over 24h)
int MinutesBackward(DateStore &toTest){
  int currentHour = rtc.getHours();
  int hoursBehind;
  if(toTest.hour<currentHour){ //Hour is directly behind us, distance is that
    hoursBehind = currentHour - toTest.hour;
  }
  else{  //It is ahead of us so needs to wrap around
    hoursBehind = (currentHour) + (23 - toTest.hour);

  }
  return hoursBehind;

  return 1;
}

bool ShouldBeOnNow(){
  int closestEnable = 30001;
  int closestDisable = 30000;

  if(startTime1.enabled){
    int enableDist = MinutesBackward(startTime1);
    int disableDist = MinutesBackward(stopTime1);
    if(enableDist < closestEnable) closestEnable = enableDist;
    if(disableDist < closestDisable) closestDisable = disableDist;
  }
  if(startTime2.enabled){
    int enableDist = MinutesBackward(startTime2);
    int disableDist = MinutesBackward(stopTime2);
    if(enableDist < closestEnable) closestEnable = enableDist;
    if(disableDist < closestDisable) closestDisable = disableDist;
  }
  if(closestEnable < closestDisable){ //Previous action was to enable output, so it shoud be enabled
    return true;
  }
  else{//Previous action was to disable output, so it shoud be enabled
    return false;
  }
}

void CheckOutputs(){
  static unsigned long outputTimer = 0;

  if(millis() - outputTimer >= 100){
    outputTimer = millis();
    digitalWrite(relay1Pin, ShouldBeOnNow());
  }
}
