#ifndef JOYHANDLER_H
#define JOYHANDLER_H
#include <Arduino.h>

class State{
  public:
    State();
    void Reset();
    bool state;
    bool repeatArmed;
    bool repeating;
    uint32_t repeatTimer;
};

class JoyHandler{
  public:
    JoyHandler(uint32_t repeatDelay, uint32_t repeatMs);  //Analog pin joy is connected to, time to start repeating, repeat interval
    void Update(int buttonADC);
    void SetRepeatMs(uint32_t);
    
    bool Up();  //These return true depending if joystick is in the position at the moment
    bool Down();
    bool Left();
    bool Right();
    bool Pressed();
    bool Released();

    bool UpR();  //These return true depending if joystick is in the position at the moment, and repeat after a short delay if joy remains there
    bool DownR();
    bool LeftR();
    bool RightR();
    bool PressedR();
    
    bool UpEvent();  //These return last non-idle position joystick was in once, and reset after.
    bool DownEvent();
    bool LeftEvent();
    bool RightEvent();
    bool PressedEvent();

  private:
    bool _Repeater(State &input);  //Generic repeater used by the repeating functions, takes in a reference to state
    uint32_t _timer; //Updated when updating joy
    uint32_t _repeatTimer;
    State up;
    State down;
    State left;
    State right;
    State pressed;
    State released;

    uint32_t _repeatDelay;
    uint32_t _repeatMs;
    bool _event;
  
};





#endif
