#include <Wire.h>              //I2C väylä näytölle
#include <Adafruit_GFX.h>      //Grafiikkakirjasto
#include <Adafruit_SSD1306.h>  //Näytön ajurikirjasto
#include <OneWire.h>
#include <DallasTemperature.h>

const int buttonPin = PA0;

const int dallasPin = PB10;
const int blinkPin = PC13;
const int auxLedPin = PA1;
const int relay1Pin = PB5;
const int relay2Pin = PB4;

const int freq = 256;   //Soft pwm frequency in hz
const int steps = 256;  //How many steps in the pwm
float value = 10;
int deviceCount = 0;
float tempC;

Adafruit_SSD1306 display(-1);
OneWire oneWire(dallasPin);	
DallasTemperature sensors(&oneWire);

void setup() {
  // put your setup code here, to run once:
  pinMode(blinkPin, OUTPUT);
  pinMode(auxLedPin, OUTPUT);
  pinMode(relay1Pin, OUTPUT);
  pinMode(relay2Pin, OUTPUT);
  pinMode(relay2Pin, OUTPUT);

  Wire.begin();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //Osoite on joko 0x3H tai 0x3C, usein valittavissa näytön takaa.
  display.setRotation(2);

  //rateaft = Wire1.i2c->currentRate;
  display.fillScreen(0);       //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
  display.setCursor(0, 0);   //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
  display.setTextColor(1, 0);  //Tekstin väri
  display.setTextSize(1);      //Tekstin koko, perusfontin koon moninkertana

  sensors.begin();
  // locate devices on the bus
  display.print("Found ");
  deviceCount = sensors.getDeviceCount();
  display.print(deviceCount, DEC);
  display.println(" devices.");

  display.display();

  delay(1000);

  
}


  

void loop() {

  display.fillScreen(BLACK);
  display.setCursor(0, 0);

    // Send command to all the sensors for temperature conversion
  //  deviceCount = sensors.getDeviceCount();
  sensors.requestTemperatures(); 
  
  // Display temperature from each sensor
  for (int i = 0;  i < deviceCount;  i++)
  {
   // DeviceAddress tempDeviceAddress;
    //sensors.getAddress(tempDeviceAddress, i);
    //printAddress(tempDeviceAddress);

    display.print(" ");
    display.print(i+1);
    display.print(" ");
    tempC = sensors.getTempCByIndex(i);
    display.println(tempC,1);
  }
  display.display();
  delay(1000);



  

}

// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) Serial.print("0");
    display.print(deviceAddress[i], HEX);
  }
}


