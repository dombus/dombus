#include <Wire.h>              //I2C väylä näytölle
#include <Adafruit_GFX.h>      //Grafiikkakirjasto
#include <Adafruit_SSD1306.h>  //Näytön ajurikirjasto

#include "JoyHandler.h"

const int buttonPin = PA0;

const int dallasPin = PB10;
const int fan1TachoPin = PB12;
const int fan2TachoPin = PB13;
const int fan1PWMPin = PB9;
const int fan2PWMPin = PB8;
const int raspifanPWMPin = PB0;
const int raspifanLEDPin = PB1;
const int blinkPin = PC13;
const int relay1Pin = PB5;
const int relay2Pin = PB4;

const int fan1PWMChannel = 4;
const int fan2PWMChannel = 3;
const int raspifanPWMChannel = 3;

const int freq = 256;   //Soft pwm frequency in hz
const int steps = 256;  //How many steps in the pwm
float value = 10;
int deviceCount = 0;
float tempC;
volatile unsigned int fan1Count = 0;
volatile unsigned int fan1Stamp = 0;
volatile unsigned int fan1Time = 0;

volatile unsigned int fan2Count = 0;
volatile unsigned int fan2Stamp = 0;
volatile unsigned int fan2Time = 0;

uint8_t fan1PWM = 0;
uint8_t fan2PWM = 13;

bool selected1 = true;

Adafruit_SSD1306 display(-1);
//HardwareTimer *MyTim = new HardwareTimer(TIM4);  // TIM3 is MCU hardware peripheral instance, its definition is provided in CMSIS
HardwareTimer *RaspiTim = new HardwareTimer(TIM2);
JoyHandler Joystick(300, 10);                    //Controls joystick. (repeat start delay, repeat delay)

void setup() {
  // put your setup code here, to run once:
  pinMode(blinkPin, OUTPUT);
  pinMode(relay1Pin, OUTPUT);
  pinMode(relay2Pin, OUTPUT);
  pinMode(raspifanLEDPin, OUTPUT);

  //pinMode(fan1TachoPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(fan1TachoPin), Fan1Tacho, FALLING);
  attachInterrupt(digitalPinToInterrupt(fan2TachoPin), Fan2Tacho, FALLING);

//  MyTim->setMode(fan1PWMChannel, TIMER_OUTPUT_COMPARE_PWM1, fan1PWMPin);
//  MyTim->setMode(fan2PWMChannel, TIMER_OUTPUT_COMPARE_PWM1, fan2PWMPin);
//  MyTim->setOverflow(25000, HERTZ_FORMAT);                                    // 25 kHz
//  MyTim->setCaptureCompare(fan1PWMChannel, fan1PWM, PERCENT_COMPARE_FORMAT);  // 50%
//  MyTim->setCaptureCompare(fan2PWMChannel, fan2PWM, PERCENT_COMPARE_FORMAT);  // 50%
//  MyTim->resume();

    RaspiTim->setMode(2, TIMER_OUTPUT_COMPARE_PWM1, PA1);
    RaspiTim->setPWM(2, PA1, 40, fan1PWM, NULL, NULL); // No callback required, we can   simplify the function call
    //RaspiTim->setPWM(channel, pin, 25000, 10); // 5 Hertz, 10% dutycycle
//  RaspiTim->setOverflow(25000, HERTZ_FORMAT);  
//  RaspiTim->setCaptureCompare(raspifanPWMChannel, fan1PWM, PERCENT_COMPARE_FORMAT);
  RaspiTim->resume();

  Wire.begin();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //Osoite on joko 0x3H tai 0x3C, usein valittavissa näytön takaa.
  display.setRotation(2);

  //rateaft = Wire1.i2c->currentRate;
  display.fillScreen(0);       //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
  display.setCursor(0, 0);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
  display.setTextColor(1, 0);  //Tekstin väri
  display.setTextSize(1);      //Tekstin koko, perusfontin koon moninkertana

  // locate devices on the bus
  display.print("Starting ");

  display.display();

  delay(1000);
}




void loop() {

  AnalogHandler();
  PwmChanger();

  display.fillScreen(BLACK);
  display.setCursor(0, 0);

  display.print("Tacho1: ");
  display.println(fan1Count);

  display.print("RPM1:   ");
  display.println((1000000.0 / ((float)fan1Time)) * 60.0 / 2.0, 0);
  display.setCursor(90, 8);
  display.print(selected1 ? "p* " : "p: ");
  display.println(fan1PWM);

  display.print("Tacho2: ");
  display.println(fan2Count);

  display.print("RPM2:   ");
  display.print((1000000.0 / ((float)fan2Time)) * 60.0 / 2.0, 0);
  display.setCursor(90, 24);
  display.print(selected1 ? "p: " : "p* ");
  display.println(fan2PWM);

  display.display();
  delay(100);
}

// function to print a device address
void Fan1Tacho() {
  fan1Count++;
  unsigned int nowTime = micros();
  fan1Time = nowTime - fan1Stamp;
  fan1Stamp = nowTime;
}

void Fan2Tacho() {
  fan2Count++;
  unsigned int nowTime = micros();
  fan2Time = nowTime - fan2Stamp;
  fan2Stamp =

    nowTime;
}

void AnalogHandler() {
  static unsigned int anaStamp = 0;

  if (millis() - anaStamp > 10) {
    Joystick.Update(analogRead(buttonPin));
    anaStamp = millis();
  }
}

void PwmChanger() {
  
  if (Joystick.Pressed()) {
    selected1 = !selected1;
  }
  if (selected1) {
    if (Joystick.LeftR()) {
      fan1PWM -= 1;
      //MyTim->setCaptureCompare(fan1PWMChannel, fan1PWM, PERCENT_COMPARE_FORMAT);
      RaspiTim->setCaptureCompare(2, fan1PWM, PERCENT_COMPARE_FORMAT);
    }
    if (Joystick.RightR()) {
      fan1PWM += 1;
      //MyTim->setCaptureCompare(fan1PWMChannel, fan1PWM, PERCENT_COMPARE_FORMAT);
      RaspiTim->setCaptureCompare(2, fan1PWM, PERCENT_COMPARE_FORMAT);
    }
  } else {
    if (Joystick.LeftR()) {
      fan2PWM -= 1;
      //MyTim->setCaptureCompare(fan2PWMChannel, fan2PWM, PERCENT_COMPARE_FORMAT);
    }
    if (Joystick.RightR()) {
      fan2PWM += 1;
      //MyTim->setCaptureCompare(fan2PWMChannel, fan2PWM, PERCENT_COMPARE_FORMAT);
    }
  }
  if(Joystick.Down()){
    fan1PWM = 0;
    fan2PWM = 0;
    //MyTim->setCaptureCompare(fan1PWMChannel, fan1PWM, PERCENT_COMPARE_FORMAT);
    //MyTim->setCaptureCompare(fan2PWMChannel, fan2PWM, PERCENT_COMPARE_FORMAT);
    RaspiTim->setCaptureCompare(2, fan1PWM, PERCENT_COMPARE_FORMAT);
  }
  digitalWrite(relay1Pin, fan1PWM > 100);
  digitalWrite(relay2Pin, fan2PWM > 100);
}
