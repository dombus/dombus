/*
   RadioLib SX126x Transmit Example

   This example transmits packets using SX1262 LoRa radio module.
   Each packet contains up to 256 bytes of data, in the form of:
    - Arduino String
    - null-terminated char array (C-string)
    - arbitrary binary data (byte array)

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

#include <Wire.h>              //I2C väylä näytölle
#include <Adafruit_GFX.h>      //Grafiikkakirjasto
#include <Adafruit_SSD1306.h>  //Näytön ajurikirjasto

Adafruit_SSD1306 display(-1);

// include the library
#include <RadioLib.h>

int globalLine = 0;
int globalCol = 0;

// SX1262 has the following connections:
// NSS pin:   PB3
// DIO1 pin:  PB2
// NRST pin: PA15
// BUSY pin:  PA4


//SPIClass SPI_1(PA7, PA6, PA5);
//SPISettings spiSettings(2000000, MSBFIRST, SPI_MODE0);
//SX1278 radio = new Module(cs, irq, rst, gpio, spi, spiSettings);

LLCC68 radio = new Module(PB3, PB2, PA15, PA4);
//LLCC68 radio = new Module(PB3, PB2, PA15, PA4, SPI_1, spiSettings);


void setup() {
  Wire.begin();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //Osoite on joko 0x3H tai 0x3C, usein valittavissa näytön takaa.
  display.setRotation(2);

  display.fillScreen(0);       //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
  display.setCursor(0, 0);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
  display.setTextColor(1, 0);  //Tekstin väri
  display.setTextSize(1);      //Tekstin koko, perusfontin koon moninkertana

  // locate devices on the bus
  display.print("Starting ");

  display.display();


  //SPI_1.begin();

  // initialize SX1262 with default settings
  Pront(F("init"));
  //freq = 434.0,bw = 125.0, sf = 9, cr = 7, syncWord = RADIOLIB_SX126X_SYNC_WORD_PRIVATE, power = 10, preambleLength = 8, tcxoVoltage = 1.6, useRegulatorLDO = false);
  int state = radio.begin(434.0, 125.0, 9, 7,RADIOLIB_SX126X_SYNC_WORD_PRIVATE, 0, 8, 0.0, false);
  if (state == RADIOLIB_ERR_NONE) {
    Pront(F("success!"));
  } else {
    Pront(F("failed, "));
    Pront(state);
    while (true);
  }

  // some modules have an external RF switch
  // controlled via two pins (RX enable, TX enable)
  // to enable automatic control of the switch,
  // call the following method
  // RX enable:   PB14
  // TX enable:   PB15
  
    radio.setRfSwitchPins(PB14, PB15);
    //radio.setOutputPower(-9);
  
}

void loop() {
  Pront(F("TP..."));

  // you can transmit C-string or Arduino string up to
  // 256 characters long
  // NOTE: transmit() is a blocking method!
  //       See example SX126x_Transmit_Interrupt for details
  //       on non-blocking transmission method.
  int state = radio.transmit("Hello World!");

  // you can also transmit byte array up to 256 bytes long
  /*
    byte byteArr[] = {0x01, 0x23, 0x45, 0x56, 0x78, 0xAB, 0xCD, 0xEF};
    int state = radio.transmit(byteArr, 8);
  */

  if (state == RADIOLIB_ERR_NONE) {
    // the packet was successfully transmitted
    //Pront(F("success!"));

    // print measured data rate
    Pront(F("Datarate:"));
    Pront(radio.getDataRate());

  } else if (state == RADIOLIB_ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    Pront(F("too long!"));

  } else if (state == RADIOLIB_ERR_TX_TIMEOUT) {
    // timeout occured while transmitting packet
    Pront(F("timeout!"));

  } else {
    // some other error occurred
    Pront(F("failed, "));
    Pront(state);

  }

  // wait for a second before transmitting again
  delay(1000);
}

template <class T>
void Pront(T thing) {
  display.setCursor(globalCol * 64, globalLine * 8);
  display.fillRect(globalCol * 64 , globalLine * 8, 64, 8, 0);
  display.print(thing);
  display.drawPixel(globalCol * 64 +62, globalLine * 8 -4, 1);
  display.display();
  globalLine++;
  if (globalLine > 3) {
    globalLine = 0;
    globalCol ++;
  }
  if (globalCol > 1) {
    globalLine = 0;
    globalCol = 0;
  }
}
