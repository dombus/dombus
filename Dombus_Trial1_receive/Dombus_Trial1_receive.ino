/*
   RadioLib SX126x Receive Example

   This example listens for LoRa transmissions using SX126x Lora modules.
   To successfully receive data, the following settings have to be the same
   on both transmitter and receiver:
    - carrier frequency
    - bandwidth
    - spreading factor
    - coding rate
    - sync word
    - preamble length

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/
#include <Wire.h>              //I2C väylä näytölle
#include <Adafruit_GFX.h>      //Grafiikkakirjasto
#include <Adafruit_SSD1306.h>  //Näytön ajurikirjasto

Adafruit_SSD1306 display(-1);

// include the library
#include <RadioLib.h>

//HardwareSerial Serial2(USART2);
HardwareSerial Serial2(PA3, PA2);

int globalLine = 0;
int globalCol = 0;

// == Inputs ==
const int buttonADCPin = PA0;
const int dallasPin = PB10;
const int fan1TachoPin = PB12;
const int fan2TachoPin = PB13;

const int Opto1Pin = PC15;
const int Opto2Pin = PC14;

// == Comms ==
const int LoRa_RXEN = PB14;
const int LoRa_TXEN = PB15;
const int LoRa_DIO1 = PB2;
const int LoRa_DIO2 = PA11;
const int LoRa_NRST = PA15;
const int LoRa_BUSY = PA4;
const int LoRa_NSS = PB3;
const int LoRa_MOSI = PA7; //"SPI1"
const int LoRa_MISO = PA6; //"SPI1"
const int LoRa_SCK = PA5;  //"SPI1"

const int RS485_RE_DE = PA8;
const int RS485_RX = PA9; //"Serial 1"
const int RS485_TX = PA10; //"Serial 1"
const int RX2Pin = PA3;  //"Serial 2"
const int TX2Pin = PA2;  //"Serial 2"

// == Outputs ==
const int fan1PWMPin = PB9;
const int fan2PWMPin = PB8;
const int ledPin = PC13;
const int relay1Pin = PB5;
const int relay2Pin = PB4;
const int AUX1Pin = PB1;
const int AUX2Pin = PB0;
const int AUX3PinLed = PA1;
const int WS2813_data = PA12;

const int fan1PWMChannel = 4;
const int fan2PWMChannel = 3;

// SX1262 has the following connections:
// NSS pin:   PB3
// DIO1 pin:  PB2
// NRST pin: PA15
// BUSY pin:  PA4
LLCC68 radio = new Module(LoRa_NSS, LoRa_DIO1, LoRa_NRST, LoRa_BUSY);
bool receivedFlag = false;

struct __attribute__((packed, aligned(4))) EM340_struct{
  int32_t V_L1;
  int32_t V_L2;
  int32_t V_L3;
  int32_t A_L1;
} EM340;

byte transmitBuf[16] = {0};

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

// or using CubeCell
//SX1262 radio = new Module(RADIOLIB_BUILTIN_MODULE);

void setup() {
  Serial2.begin(9600);
  Wire.begin();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //Osoite on joko 0x3H tai 0x3C, usein valittavissa näytön takaa.
  display.setRotation(2);

  display.fillScreen(0);       //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
  display.setCursor(0, 0);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
  display.setTextColor(1, 0);  //Tekstin väri
  display.setTextSize(1);      //Tekstin koko, perusfontin koon moninkertana

  // locate devices on the bus
  display.print("Starting ");

  display.display();

  // initialize SX1262 with default settings
  Pront(F("Init.."));
  //int state = radio.begin();
  int state = radio.begin(434.0, 125.0, 9, 7,RADIOLIB_SX126X_SYNC_WORD_PRIVATE, -9, 8, 0.0, false);
  if (state == RADIOLIB_ERR_NONE) {
    Pront(F("success!"));
  } else {
    Pront(F("failed, code "));
    Pront(state);
    while (true);
  }

  radio.setRfSwitchPins(LoRa_RXEN, LoRa_TXEN);
  radio.setDio1Action(setFlag);

  state = radio.startReceive();
    if (state == RADIOLIB_ERR_NONE) {
    Pront(F("success!"));
  } else {
    Pront(F("failed, code "));
    Pront(state);
    while (true);
  }
}

void loop() {
  //Pront(F("Wait.. "));

  // you can receive data as an Arduino String
  // NOTE: receive() is a blocking method!
  //       See example ReceiveInterrupt for details
  //       on non-blocking reception method.
  //String str;
  //int state = radio.receive(str);
  //byte byteArr[16];
  //int state = radio.receive(byteArr, 16);
  //int state = radio.receive((byte*)&EM340, sizeof(EM340));
  if(receivedFlag){
    receivedFlag = false;
    int state = GetAnything(EM340, sizeof(EM340_struct));
  
    // you can also receive data as byte array
    /*
      byte byteArr[8];
      int state = radio.receive(byteArr, 8);
    */
  
    if (state == RADIOLIB_ERR_NONE) {
      // packet was successfully received
      //Pront(F("success!"));
  
      // print the data of the packet
      //Pront(F("Data: "));
      Pront(EM340.V_L1);
      Pront(EM340.V_L2);
      Pront(EM340.V_L3);
      Pront(EM340.A_L1);
      Serial2.print((float)EM340.V_L1/10.0f); Serial2.print('\t');
      Serial2.print((float)EM340.V_L2/10.0f); Serial2.print('\t');
      Serial2.print((float)EM340.V_L3/10.0f); Serial2.print('\t');
      Serial2.println((float)EM340.A_L1/10.0f);
      //Pront(byteArr[2]);
  
      // print the RSSI (Received Signal Strength Indicator)
      // of the last received packet
      Pront(F("RSSI:"));
      Pront(radio.getRSSI());
  
      // print the SNR (Signal-to-Noise Ratio)
      // of the last received packet
      Pront(F("SNR: "));
      Pront(radio.getSNR());
      state = radio.startReceive();
  
    } else if (state == RADIOLIB_ERR_RX_TIMEOUT) {
      // timeout occurred while waiting for a packet
      Pront(F("timeout!"));
  
    } else if (state == RADIOLIB_ERR_CRC_MISMATCH) {
      // packet was received, but is malformed
      Pront(F("CRC error!"));
  
    } else {
      // some other error occurred
      Pront(F("fail, c "));
      Pront(state);
  
    }
  }
}

void setFlag(void) {
  // we got a packet, set the flag
  receivedFlag = true;
}

template <class T>
void Pront(T thing) {
  display.setCursor(globalCol * 64, globalLine * 8);
  display.fillRect(globalCol * 64 , globalLine * 8, 64, 8, 0);
  display.print(thing);
  display.drawPixel(globalCol * 64 +62, globalLine * 8 -4, 1);
  display.display();
  //delay(500);
  globalLine++;
  if (globalLine > 3) {
    globalLine = 0;
    globalCol ++;
  }
  if (globalCol > 1) {
    globalLine = 0;
    globalCol = 0;
  }
}

template <class T> int GetAnything(const T& value, unsigned int len){
    byte* p = (byte*)(void*)&value;
    //byte *arr = new byte(len);
    unsigned int i;
    
    int state = radio.readData(transmitBuf, len);
    for (i = 0; i < len; i++){ //Read until struct is full
      *p++ = transmitBuf[i];
    }
    //delete arr;
    return state;
}

template <class T> int SendAnythingLoRa(const T& value, unsigned int len){
    const byte* p = (const byte*)(const void*)&value;
    byte *arr = new byte(len);
    unsigned int i;
    
    for (i = 0; i < len; i++){
      const byte b = *p;
      arr[i] = b;
      p++;
    }
    int state = radio.transmit(arr, len);
    delete arr;
    return state;
}
